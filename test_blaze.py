import blaze as bl
import pandas as pd
import numpy as np
import sklearn as sk
from odo import odo

"""
data = pd.read_csv("FinalSet/transaction_resaved.csv", index_col=False)
bdata = bl.Data(data)
"""
"""
sber_x = bl.dshape("var * {'0': string, '1': string, '2': string, '3': string, '4': float64, '5': int64, '6': string}")


odo('FinalSet/transaction_resaved.csv', 'sber_x.hdf5::/mydataset', dshape=sber_x)  


sber_x = bl.dshape("var * {'0': string, '1': string, '2': string, '3': string, '4': float64, '5': int64, '6': string}")
dshape=sber_x
"""

bdata = bl.data('FinalSet/transaction_resaved.csv')

"""
columns=['INN_R','KPP_R','INN_P','KPP_P','Money','Time','Reference']
"""
print ("Data structure itself")
print (bdata)

print ("Fields avaliable in data strcuture")
print (bdata.fields)

print ("Sample data")
print (bl.compute(bdata.head()))

print ("Some experimets")
shape = bdata.dshape.parameters[-1].dict
for k in shape:
    print (k)
    
print ("Relabel")
bdata.relabel('0'='SPECIES', '1'='LENGTH')

print ("Fields after renaming")
print (bdata.fields)

print ("Top 10 elemets")
print (bdata.peek())

print ("Schema")
print (bdata.schema)

print ("Resources")
print (bdata._resources().values())
        
print ()